[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

# Role Based Access Control Lists

Disclaimer: this is a work in progress based on `typeorm` which is also a work in progress

TL;DR:

```typescript
import {Types, Permissions, Resources, Roles, Groups, Principals, entities} from '@any4/acl'
import {createConnection} from 'typeorm'
import 'reflect-metadata'
require('sqlite3')

createConnection({type: 'sqlite', database: ':memory:', entities}).then(async connection => {
  await connection.transaction(em => em.getCustomRepository(Roles)
    .assert('pirate', [{permission: 'drink', type: 'liquor'}, {permission: 'plunder', type: 'booty'}]))
  await connection.transaction(em => em.getCustomRepository(Groups)
    .add('spirit', [{resource: 'rum', type: 'liquor'}]))
  await connection.transaction(em => em.getCustomRepository(Groups)
    .add('cash', [{resource: 'doubloons', type: 'booty'}, {resource: 'reales', type: 'booty'}]))
  await connection.transaction(em => em.getCustomRepository(Principals)
    .grant('jack-sparrow', [{role: 'pirate', group: 'spirit'}, {role: 'pirate', group: 'cash'}]))

  await connection.transaction(em => em.getCustomRepository(Principals)
    .check('jack-sparrow', 'drink', 'liquor', 'rum'))
  await connection.transaction(em => em.getCustomRepository(Principals)
    .check('jack-sparrow', 'plunder', 'booty', 'doubloons'))
}).catch(console.log)
```

Library for implementing access control lists. There are 5 main domain models supporting authorization: types,
resources, groups (which aggregate resources), permissions, and roles (which aggregate permissions). Resources and
permissions are each namespaced within a type, so for example, permission `'eat'` over type `'veggies'` and permission
`'eat'` over type `'hot-dogs'` are in fact two different permissions. For a principal to be allowed a permission over a
resource, the principal must be granted a role with this permission over a group which contains this resource.
Naturally, if a user is granted `eat` permission for type `veggies` he will not be permitted to `eat` resource
`cheese-dog` of type `hot-dogs`. You may create types, permissions and resources explicitly or you can `assert`
permissions into roles and `add` resources into groups and they'll be created if they do not exist
