import {AbstractRepository, EntityRepository} from "typeorm";
import {Type} from '../entities'

@EntityRepository(Type)
export class Types extends AbstractRepository<Type> {
  async create(name: string): Promise<Type> {
    let type = await this.repository.findOne({where: {name}})
    if (!type) type = await this.repository.save(this.repository.create({name}))
    return type
  }
}
