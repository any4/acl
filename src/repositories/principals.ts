import {AbstractRepository, EntityRepository} from 'typeorm'
import {Grant, Principal} from '../entities'
import {Groups} from './groups'
import {Roles} from './roles'

@EntityRepository(Principal)
export class Principals extends AbstractRepository<Principal> {
  async check(name: string, type: string, resource: string, permission: string): Promise<boolean> {
    return this.repository.createQueryBuilder('principal')
      .where('principal.name = :name', {name})
      .innerJoinAndSelect('principal.grants', 'grant')
      .innerJoinAndSelect('grant.role', 'role')
      .innerJoinAndSelect('role.permissions', 'permission', 'permission.name = :permission', {permission})
      .innerJoinAndSelect('permission.type', 'ptype', 'ptype.name = :type', {type})
      .innerJoinAndSelect('grant.group', 'group')
      .innerJoinAndSelect('group.resources', 'resource', 'resource.name = :resource', {resource})
      .innerJoinAndSelect('resource.type', 'rtype', 'rtype.name = :type', {type})
      .getCount().then(count => count && true || false)
  }

  async grant(name: string, grants: {group: string, role: string}[]): Promise<void> {
    const principal = await this.repository.findOne({where: {name}}) || this.repository.create({name, grants: []})
    principal.grants.push(...await Promise.all(grants.filter(({role, group}) => {
      return !principal.grants.find(({role: {name: rn}, group: {name: gn}}) => role === rn && group === gn)
    }).map(async ({role: rn, group: gn}) => {
      const [role, group] = await Promise.all([
        this.manager.getCustomRepository(Roles).find(rn), this.manager.getCustomRepository(Groups).find(gn)
      ])
      return this.manager.save(this.manager.create(Grant, {role, group}))
    })))
    await this.manager.save(principal)
  }

  async revoke(name: string, grants: {group: string, role: string}[]): Promise<void> {
    const principal = await this.repository.findOne({where: {name}})
    if (principal) {
      principal.grants = principal.grants.filter(({group: {name: gn}, role: {name: rn}}) => {
        return !grants.find(({group, role}) => group === gn && role === rn)
      })
      await (principal.grants.length ? this.repository.save(principal) : this.repository.remove(principal))
    }
  }
}
