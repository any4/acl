import {AbstractRepository, EntityRepository} from 'typeorm'
import {Group} from '../entities'
import {Resources} from './resources'

@EntityRepository(Group)
export class Groups extends AbstractRepository<Group> {
  async find(name: string): Promise<Group> {
    return this.repository.findOneOrFail({where: {name}})
  }

  async add(name: string, resources: {resource: string, type: string}[]): Promise<void> {
    const group = await this.repository.findOne({where: {name}}) || this.repository.create({name, resources: []})
    group.resources.push(...await Promise.all(resources.filter(({resource, type}) => {
      return !group.resources.find(( {name: rn, type: {name: tn}}) => resource === rn && type === tn)
    }).map(async ({resource, type}) => this.manager.getCustomRepository(Resources).create(resource, type))))
    await this.repository.save(group)
  }

  async drop(name: string, resources: {resource: string, type: string}[]): Promise<void> {
    const group = await this.repository.findOne({where: {name}})
    if (group) {
      group.resources = group.resources.filter(({name: rn, type: {name: tn}}) => {
        return !resources.find(({resource, type}) => rn === resource && type === tn)
      })
      await (group.resources.length ? this.repository.save(group) : this.repository.remove(group))
    }
  }
}
