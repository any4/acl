import {AbstractRepository, EntityRepository} from 'typeorm'
import {Role} from '../entities'
import {assign} from 'lodash'
import {Permissions} from './permissions'

@EntityRepository(Role)
export class Roles extends AbstractRepository<Role> {
  async assert(name: string, permissions: {permission: string, type: string}[]): Promise<Role> {
    const role = await this.repository.findOne({where: {name}}) || this.repository.create()
    return this.repository.save(assign(role, {
      name, permissions: (role.permissions || []).concat(await Promise.all(permissions
        .map(({permission, type}) => {
          return this.manager.getCustomRepository(Permissions).create(permission, type)
        })))
    }))
  }

  async find(name: string): Promise<Role> {
    return this.repository.findOneOrFail({where: {name}})
  }
}
