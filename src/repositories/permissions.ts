import {AbstractRepository, EntityRepository} from 'typeorm'
import {Permission} from '../entities'
import {Types} from './types'

@EntityRepository(Permission)
export class Permissions extends AbstractRepository<Permission> {
  async create(name: string, type: string): Promise<Permission> {
    const resource = await this.createQueryBuilder('permission')
      .innerJoinAndSelect('permission.type', 'type', 'type.name = :type', {type})
      .where('permission.name = :name', {name})
      .getOne()
    return resource || await this.repository.save(this.repository.create({
      name, type: await this.manager.getCustomRepository(Types).create(type)
    }))
  }
}
