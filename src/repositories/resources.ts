import {AbstractRepository, EntityRepository} from 'typeorm'
import {Group, Resource} from '../entities'
import {Types} from './types'

@EntityRepository(Resource)
export class Resources extends AbstractRepository<Resource> {
  async create(name: string, type: string): Promise<Resource> {
    const resource = await this.createQueryBuilder('resource')
      .innerJoinAndSelect('resource.type', 'type', 'type.name = :type', {type})
      .where('resource.name = :name', {name})
      .getOne()
    return resource || await this.repository.save(this.repository.create({
      name, type: await this.manager.getCustomRepository(Types).create(type)
    }))
  }

  async remove(name: string, type: string): Promise<void> {
    const resource = await this.createQueryBuilder('resource')
      .innerJoinAndSelect('resource.type', 'type', 'type.name = :type', {type})
      .where('resource.name = :name', {name})
      .getOne()
    const groups = await this.createQueryBuilderFor(Group, 'group')
      .leftJoin('group.resources', 'resource', 'resource.name = :name', {name})
      .getMany().then(groups => this.manager.findByIds(Group, groups.map(({id}) => id)))
    await Promise.all(groups.map(async group => {
      group.resources = group.resources.filter(({id}) => this.manager.getId(resource) !== id)
      return group.resources.length ? this.manager.save(group) : this.manager.remove(group)
    }))
    if (resource) await this.manager.remove(resource)
  }
}
