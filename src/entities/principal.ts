import {Column, Entity, Index, OneToMany, PrimaryGeneratedColumn} from 'typeorm'
import {Grant} from './grant'

@Entity()
@Index(['name'], {unique: true})
export class Principal {
  @PrimaryGeneratedColumn()
  private id: number

  @Column({type: 'varchar'})
  name: string

  @OneToMany(type => Grant, grant => grant.principal, {eager: true})
  grants: Grant[]
}
