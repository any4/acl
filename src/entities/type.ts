import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm'

@Entity()
@Index(['name'], {unique: true})
export class Type {
  @PrimaryGeneratedColumn()
  private id: number

  @Column({type: 'varchar'})
  name: string
}
