import {Column, Entity, Index, JoinTable, ManyToMany, PrimaryGeneratedColumn} from 'typeorm'
import {Permission} from './permission'

@Entity()
@Index(['name'], {unique: true})
export class Role {
  @PrimaryGeneratedColumn()
  private id: number

  @Column({type: 'varchar'})
  name: string

  @ManyToMany(type => Permission, {eager: true})
  @JoinTable()
  permissions: Permission[]
}
