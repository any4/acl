import {Column, Entity, Index, JoinTable, ManyToMany, PrimaryGeneratedColumn} from 'typeorm'
import {Resource} from './resource'

@Entity()
@Index(['name'], {unique: true})
export class Group {
  @PrimaryGeneratedColumn()
  private id: number

  @Column({type: 'varchar'})
  name: string

  @ManyToMany(type => Resource, {eager: true})
  @JoinTable()
  resources: Resource[]
}
