import {Entity, JoinTable, ManyToOne, PrimaryGeneratedColumn} from 'typeorm'
import {Role} from './role'
import {Group} from './group'
import {Principal} from './principal'

@Entity()
export class Grant {
  @PrimaryGeneratedColumn()
  private id: number

  @ManyToOne(type => Role, {eager: true})
  @JoinTable()
  role: Role

  @ManyToOne(type => Group, {eager: true})
  @JoinTable()
  group: Group

  @ManyToOne(type => Principal, principal => principal.grants, {onDelete: 'CASCADE'})
  principal: Principal
}
