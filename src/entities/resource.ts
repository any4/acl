import {Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn} from 'typeorm'
import {Type} from './type'

@Entity()
@Index(['name', 'type'], {unique: true})
export class Resource {
  @PrimaryGeneratedColumn()
  private id: number

  @Column({type: 'varchar'})
  name: string

  @ManyToOne(type => Type, {eager: true})
  type: Type
}
