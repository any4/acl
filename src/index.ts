import * as dto from './entities'
import {values} from 'lodash'

export * from './entities'
export * from './repositories'
export const entities = values(dto)
