import {createConnection, getConnectionOptions, Connection, EntityManager} from 'typeorm'
import {entities} from '../index'
import {ConnectionOptions} from 'typeorm/connection/ConnectionOptions'
import {merge} from 'lodash'
import 'reflect-metadata'

let connection: Connection

global.before(async () => {
  const config: ConnectionOptions = await getConnectionOptions()
  const options = merge({}, config, {entities})
  connection = await createConnection(options)
})

global.beforeEach(async () => {
  await connection.dropDatabase()
  await connection.synchronize()
})

export function it(title: string, fn: (em: EntityManager) => Promise<any>): Mocha.Test {
  return global.it(title, async () => connection.transaction(fn))
}

export function before(fn: (em: EntityManager) => Promise<any>): void {
  global.before(async () => connection.transaction(fn))
}

export function beforeEach(fn: (em: EntityManager) => Promise<any>): void {
  global.beforeEach(async () => connection.transaction(fn))
}

export function after(fn: (em: EntityManager) => Promise<any>): void {
  global.after(async () => connection.transaction(fn))
}

export function afterEach(fn: (em: EntityManager) => Promise<any>): void {
  global.afterEach(async () => connection.transaction(fn))
}
