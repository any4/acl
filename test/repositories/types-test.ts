import {beforeEach, it, afterEach} from '../connection'
import 'should'
import {Type, Types} from '../../src'

describe('Resource type', () => {
  describe('create', () => {
    describe('a new type', () => {
      it('should resolve', em => em.getCustomRepository(Types).create('car'))
    })

    describe('an existing type', () => {
      let car

      beforeEach( async em => car = await em.save(em.create(Type, {name: 'car'})))

      it('should resolve to existing type', em => em.getCustomRepository(Types).create('car').should.be.fulfilledWith(car))
    })

    afterEach(em => em.find(Type).then(types => types.map(({name}) => name).should.eql(['car'])))
  })
})
