import {it, afterEach, beforeEach} from '../connection'
import {Group, Resource, Groups, Type} from '../../src'
import 'should'

describe('Group', () => {
  describe('add', () => {
    describe('a new group', () => {
      describe('with a new resource', () => {
        describe('with a new type', () => {
          it('should resolve', async em => await em.getCustomRepository(Groups)
            .add('luxury', [{resource: 'bmw', type: 'car'}]))
        })

        describe('with an existing type', () => {
          let type

          beforeEach(async em => type = await em.save(em.create(Type, {name: 'car'})))

          it('should resolve', async em => await em.getCustomRepository(Groups)
            .add('luxury', [{resource: 'bmw', type: 'car'}]))

          afterEach(em => em.find(Type).should.be.fulfilledWith([type]))
        })
      })

      describe('with an existing resource', () => {
        let resource

        beforeEach(async em => resource = await em.save(em.create(Resource, {
          name: 'bmw', type: await em.save(em.create(Type, {name: 'car'}))
        })))

        it('should resolve', async em => await em.getCustomRepository(Groups)
          .add('luxury', [{resource: 'bmw', type: 'car'}]))

        afterEach(em => em.find(Resource).should.be.fulfilledWith([resource]))
      })
    })

    describe('an existing group', () => {
      let group

      beforeEach(async em => group = await em.save(em.create(Group, {
        name: 'luxury', resources: [await em.save(em.create(Resource, {
          name: 'bmw', type: await em.save(em.create(Type, {name: 'car'}))
        }))]
      })))

      it('should resolve', async em => await em.getCustomRepository(Groups)
        .add('luxury', [{resource: 'bmw', type: 'car'}]))

      afterEach(async em => em.find(Group).should.be.fulfilledWith([group]))
    })
  })

  describe('find', () => {
    describe('an existing group', () => {
      beforeEach(async em => em.save(em.create(Group, {
        name: 'luxury', resources: [await em.save(em.create(Resource, {
          name: 'bmw', type: await em.save(em.create(Type, {name: 'car'}))
        }))]
      })))

      it('should resolve', async em => {
        const luxury = await em.getCustomRepository(Groups).find('luxury')
        luxury.should.have.properties({name: 'luxury'})
        luxury.should.have.property('resources').which.is.an.Array().of.length(1)
        luxury.resources[0].should.have.properties({name: 'bmw'})
        luxury.resources[0].should.have.property('type').with.properties({name: 'car'})
      })
    })

    describe('a non-existing group', () => {
      it('should reject', em => em.getCustomRepository(Groups).find('foo')
        .should.be.rejected())
    })
  })

  describe('drop', () => {
    describe('from an existing group', () => {
      let car

      beforeEach(async em => {
        car = await em.save(em.create(Type, {name: 'car'}))
        await em.save(em.create(Group, {
          name: 'luxury', resources: [await em.save(em.create(Resource, {name: 'bmw', type: car}))]
        }))
      })

      describe('an existing resource', () => {
        describe('with other affiliated resources', () => {
          beforeEach(async em => {
            const luxury = await em.findOne(Group)
            luxury.resources.push(await em.save(em.create(Resource, {name: 'audi', type: car})))
            await em.save(luxury)
          })

          it('should resolve', em => em.getCustomRepository(Groups)
            .drop('luxury', [{resource: 'bmw', type: 'car'}]))

          afterEach(async em => {
            const groups = await em.find(Group)
            groups.length.should.eql(1)
            groups[0].should.have.properties({name: 'luxury'})
            groups[0].should.have.property('resources').which.is.an.Array().with.length(1)
            groups[0].resources[0].should.have.properties({name: 'audi'})
            groups[0].resources[0].should.have.property('type').with.properties({name: 'car'})
          })
        })

        describe('without other affiliated resources', () => {
          it('should resolve', em => em.getCustomRepository(Groups)
            .drop('luxury', [{resource: 'bmw', type: 'car'}]))

          afterEach(async em => {
            await em.count(Group).should.be.fulfilledWith(0)
            const [bmw] = await em.find(Resource)
            bmw.should.have.properties({name: 'bmw'})
            bmw.should.have.property('type').with.properties({name: 'car'})
          })
        })
      })

      describe('a non-existing resource', () => {
        it('should resolve', async em => em.getCustomRepository(Groups).drop('luxury', [{
          resource: 'cadillac', type: 'car'
        }]))
      })
    })

    describe('from a non-existing group', () => {
      it('should resolve', em => em.getCustomRepository(Groups).drop('unicorns', [{
        resource: 'bob', type: 'unreal'
      }]))
    })
  })
})
