import {beforeEach, it, afterEach} from '../connection'
import 'should'
import {Type, Resource, Resources, Group} from '../../src'

describe('Resource', () => {
  describe('create', () => {
    let resource

    describe('a new resource', () => {
      describe('with a new type', () => {
        it('should resolve', async em => resource = await em.getCustomRepository(Resources)
          .create('bmw', 'car'))
      })

      describe('with an existing type', () => {
        let car

        beforeEach(async em => car = await em.save(em.create(Type, {name: 'car'})))

        it('should resolve with a resource of the existing type', async em => {
          const {type} = resource = await em.getCustomRepository(Resources).create('bmw', 'car')
          type.should.eql(car)
        })
      })
    })

    describe('an existing resource', () => {
      let bmw

      beforeEach(async em => bmw = await em.save(em.create(Resource, {
        name: 'bmw', type: await em.save(em.create(Type, {name: 'car'}))
      })))

      it('should resolve to the existing resource', async em => resource = await em.getCustomRepository(Resources)
        .create('bmw', 'car').should.be.fulfilledWith(bmw))
    })

    afterEach(async em => em.find(Resource).should.be.fulfilledWith([resource]))
  })

  describe('remove', () => {
    describe('an existing resource', () => {
      let bmw

      beforeEach(async em => bmw = await em.save(em.create(Resource, {
        name: 'bmw', type: await em.save(em.create(Type, {name: 'car'}))
      })))

      describe('unaffiliated with a group', () => {
        it('should resolve', em => em.getCustomRepository(Resources).remove('bmw', 'car'))

        afterEach(em => em.count(Resource).should.be.fulfilledWith(0))
      })

      describe('affiliated with a group', () => {
        let group

        beforeEach(async em => group = await em.save(em.create(Group, {name: 'luxury', resources: [bmw]})))

        describe('without other affiliated resources', () => {
          it('should resolve', em => em.getCustomRepository(Resources).remove('bmw', 'car'))

          afterEach(async em => {
            await em.count(Resource).should.be.fulfilledWith(0)
            await em.count(Group).should.be.fulfilledWith(0)
          })
        })

        describe('with another resource affiliated', () => {
          let audi

          beforeEach(async em => {
            group.resources.push(audi = await em
              .save(em.create(Resource, {name: 'audi', type: await bmw.type})))
            await em.save(group)
          })

          it('should resolve', em => em.getCustomRepository(Resources).remove('bmw', 'car'))

          afterEach(async em => {
            await em.count(Group).should.be.fulfilledWith(1)
            const [luxury] = await em.find(Group)
            luxury.should.have.property('resources').which.is.an.Array().with.length(1)
          })
        })
      })
    })

    describe('a non-existing type', () => {
      it('should resolve', em => em.getCustomRepository(Resources).remove('bmw', 'car'))

      afterEach(em => em.count(Resource).should.be.fulfilledWith(0))
    })

    afterEach(em => em.count(Resource, {where: {name: 'bmw'}}).should.be.fulfilledWith(0))
  })
})
