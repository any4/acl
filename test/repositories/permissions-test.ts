import {it, beforeEach, afterEach} from '../connection'
import 'should'
import {Type, Permission, Permissions} from '../../src'

describe('Permission', () => {
  describe('create', () => {
    describe('a new permission', () => {
      describe('with a new type', () => {
        it('should resolve', em => em.getCustomRepository(Permissions).create('fix', 'car'))
      })

      describe('with an existing type', () => {
        let car

        beforeEach(async em => car = await em.save(em.create(Type, {name: 'car'})))

        it('should resolve to a permission tied to existing type', async em => em.getCustomRepository(Permissions)
          .create('fix', 'car').then(fix => fix.should.have.properties({type: car})))
      })
    })

    describe('an existing permission', () => {
      let fix

      beforeEach(async em => fix = await em.save(em.create(Permission, {name: 'fix', type: await em
          .save(em.create(Type, {name: 'car'}))})))

      it('should resolve to the existing permission', em => em.getCustomRepository(Permissions)
        .create('fix', 'car').should.be.fulfilledWith(fix))
    })

    afterEach(async em => {
      const permissions = await em.find(Permission)
      permissions.map(({name}) => name).should.eql(['fix'])
    })

    afterEach(async em => {
      const types = await em.find(Type)
      types.map(({name}) => name).should.eql(['car'])
    })
  })
})
