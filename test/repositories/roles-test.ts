import {it, afterEach, beforeEach} from '../connection'
import 'should'
import {Type, Permission, Roles, Role} from '../../src'

describe('Role', () => {
  describe('assert', () => {
    let role

    describe('a new role', () => {
      describe('with a new permission', () => {
        describe('with a new type', () => {
          it('should resolve', async em => role = await em.getCustomRepository(Roles)
            .assert('mechanic', [{permission: 'fix', type: 'car'}]))
        })

        describe('with an existing type', () => {
          let car

          beforeEach(async em => car = await em.save(em.create(Type, {name: 'car'})))

          it('should resolve to a role with a permission on the existing type', async em => {
            const {permissions: [{type}]} = role = await em.getCustomRepository(Roles)
              .assert('mechanic', [{permission: 'fix', type: 'car'}])
            car.should.eql(type)
          })
        })
      })

      describe('with an existing permission', () => {
        let fix

        beforeEach(async em => fix = await em.save(em.create(Permission, {
          name: 'fix', type: await em.save(em.create(Type, {name: 'car'}))
        })))

        it('should resolve to a role with an existing permission', async em => {
          const {permissions} = role = await em.getCustomRepository(Roles)
            .assert('mechanic', [{permission: 'fix', type: 'car'}])
          permissions.should.eql([fix])
        })
      })

      afterEach(async em => {
        role.should.have.property('permissions').which.is.Array().length(1)
      })
    })

    describe('an existing role', () => {
      let mechanic

      beforeEach(async em => mechanic = await em.save(em.create(Role, {
        name: 'mechanic', permissions: [await em.save(em.create(Permission, {
          name: 'fix', type: await em.save(em.create(Type, {name: 'car'}))
        }))]
      })))

      describe('with a new permission', () => {
        describe('with a new type', () => {
          it('should resolve', async em => role = await em.getCustomRepository(Roles)
            .assert('mechanic', [{permission: 'fix', type: 'bike'}]))
        })

        describe('with an existing type', () => {
          let bike

          beforeEach(async em => bike = await em.save(em.create(Type, {name: 'bike'})))

          it('should resolve', async em => {
            role = await em.getCustomRepository(Roles).assert('mechanic', [{permission: 'fix', type: 'bike'}])
            role.permissions.find(({type: {name}}) => name === 'bike').type.should.eql(bike)
          })
        })
      })

      describe('with an existing permission', () => {
        let fix

        beforeEach(async em => fix = await em.save(em.create(Permission, {
          name: 'fix', type: await em.save(em.create(Type, {name: 'bike'}))
        })))

        it('should resolve to a role with the existing permission', async em => {
          role = await em.getCustomRepository(Roles).assert('mechanic', [{permission: 'fix', type: 'bike'}])
          role.permissions.find(({type: {name}}) => name === 'bike').should.eql(fix)
        })
      })

      afterEach(async em => {
        em.getId(role).should.eql(em.getId(mechanic))
        role.permissions.map(({name}) => name.should.eql('fix'))
        role.permissions.length.should.eql(2)
        role.permissions.find(({type: {name: type}}) => type === 'bike').should.be.ok()
      })
    })

    afterEach(async em => {
      role.should.have.properties({name: 'mechanic'})
      role.permissions.find(({type: {name: type}}) => type === 'car').should.be.ok()
    })
  })

  describe('find', () => {
    describe('an existing role', () => {
      let mechanic

      beforeEach(async em => mechanic = await em.save(em.create(Role, {
        name: 'mechanic', permissions: [await em.save(em.create(Permission, {
          name: 'fix', type: await em.save(em.create(Type, {name: 'car'}))
        }))]
      })))

      it('should resolve', em => em.getCustomRepository(Roles).find('mechanic').should.be.fulfilledWith(mechanic))
    })

    describe('a non-existing role', () => {
      it('should reject', em => em.getCustomRepository(Roles).find('wizard').should.be.rejected())
    })
  })
})
