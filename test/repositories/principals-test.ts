import {beforeEach, afterEach, it} from '../connection'
import {Group, Permission, Principal, Principals, Resource, Role, Type, Grant} from '../../src'
import 'should'

describe('Principals', () => {
  let mechanic, luxury, economy

  beforeEach(async em => {
    const car = await em.save(em.create(Type, {name: 'car'}))
    const bike = await em.save(em.create(Type, {name: 'bike'}))
    const fixcar = await em.save(em.create(Permission, {name: 'fix', type: car}))
    const fixbike = await em.save(em.create(Permission, {name: 'fix', type: bike}))
    mechanic = await em.save(em.create(Role, {name: 'mechanic', permissions: [fixcar, fixbike]}))
    const bmw = await em.save(em.create(Resource, {name: 'bmw', type: car}))
    await em.save(em.create(Resource, {name: 'bmw', type: bike}))
    const audi = await em.save(em.create(Resource, {name: 'audi', type: bike}))
    const honda = await em.save(em.create(Resource, {name: 'honda', type: car}))
    luxury = await em.save(em.create(Group, {name: 'luxury', resources: [bmw, audi]}))
    economy = await em.save(em.create(Group, {name: 'economy', resources: [honda]}))
  })

  describe('grant', () => {
    describe('a new grant', () => {
      it('should resolve', em => em.getCustomRepository(Principals)
        .grant('bob', [{role: 'mechanic', group: 'luxury'}]))

      afterEach(async em => {
        const bob = await em.findOneOrFail(Principal, {where: {name: 'bob'}})
        bob.should.be.ok()
        bob.should.have.properties({name: 'bob'})
        bob.should.have.property('grants').which.is.an.Array().length(1)
        bob.grants[0].should.have.properties({role: mechanic, group: luxury})
      })
    })
  })

  describe('revoke', () => {
    describe('an existing grant', () => {
      beforeEach(async em => {
        await em.save(em.create(Principal, {
          name: 'bob', grants: [await em.save(em.create(Grant, {role: mechanic, group: luxury}))]
        }))
      })

      it('should resolve', em => {
        return em.getCustomRepository(Principals).revoke('bob', [{group: 'luxury', role: 'mechanic'}])
      })

      afterEach(async em => {
        await em.count(Principal).should.be.fulfilledWith(0)
        await em.count(Grant).should.be.fulfilledWith(0)
      })
    })
  })

  describe('check', () => {
    beforeEach(async em => {
      await em.save(em.create(Principal, {
        name: 'bob', grants: [await em.save(em.create(Grant, {role: mechanic, group: luxury}))]
      }))
    })

    describe('with granted permissions', () => {
      it('should resolve to true', em => em.getCustomRepository(Principals)
        .check('bob', 'car', 'bmw', 'fix')
        .should.be.fulfilledWith(true))
    })

    describe('without granted permissions', () => {
      it('should resolve to false', em => em.getCustomRepository(Principals)
        .check('bob', 'car', 'honda', 'fix')
        .should.be.fulfilledWith(false))
    })
  })
})
